﻿// Homework_18v2.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

using namespace std;
class Stack
{
private:
    
    int* mas;
    int top;
    int size;
public:
    Stack(int _size) :size(_size) { mas = (int*)malloc(size * sizeof(int));  top = 0; }
    int StackSize()
    {
        return size;
    }
    //Помещаем элемент в стек

    void Push(int item)
    {
        mas[top] = item;
        top++;
    }

    //Выталкиваем элемент из стека
  
    int pop()
    {
        top--;
       return mas[top];
    }

    //Добавляем елемент в стек
   
    int AddElem(int item)
    {
        size++;
        int* new_mas = new int[size];
        for (int i = 0;i < size - 1;++i)

            new_mas[i] = mas[i];

        delete[] mas;
        mas = new_mas;
        mas[top] = item;
        top++;
        return 0;
    }
};



int main()
{
    int i, size;
    Stack st1(10);
    for (i = 0; i < 10; i++)
    {
        st1.Push(i + 5);
    }
    st1.AddElem(19);
    size = st1.StackSize();
    cout << size << endl;
    for (i = 0; i < size - 1; i++)
        cout << st1.pop() << ' ';
    cout << "\n" << endl;
    return 0;
}

// Запуск программы: CTRL+F5 или меню "Отладка" > "Запуск без отладки"
// Отладка программы: F5 или меню "Отладка" > "Запустить отладку"

// Советы по началу работы 
//   1. В окне обозревателя решений можно добавлять файлы и управлять ими.
//   2. В окне Team Explorer можно подключиться к системе управления версиями.
//   3. В окне "Выходные данные" можно просматривать выходные данные сборки и другие сообщения.
//   4. В окне "Список ошибок" можно просматривать ошибки.
//   5. Последовательно выберите пункты меню "Проект" > "Добавить новый элемент", чтобы создать файлы кода, или "Проект" > "Добавить существующий элемент", чтобы добавить в проект существующие файлы кода.
//   6. Чтобы снова открыть этот проект позже, выберите пункты меню "Файл" > "Открыть" > "Проект" и выберите SLN-файл.
